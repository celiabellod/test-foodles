def list_of_frequency_of_each_word(sentence: str, number: int) -> list:
    """
    returns a list of size `n` where each element contains
    a word and the frequency of that word in `sentence`
    This list should be sorted by decreasing frequency and
    alphabetical order in case of tie.
    """
    list_of_frequency_of_each_word = []

    split_sentence_to_uniq_words = set(sentence.split())
    word_sorted = sorted(
        split_sentence_to_uniq_words, key=lambda x: (-len(x), x)
    )

    for word in word_sorted:
        number_of_frenquency = sentence.count(word)
        if number_of_frenquency > 1:
            list_of_frequency_of_each_word.append((word, number_of_frenquency))

    return list_of_frequency_of_each_word[:number]
