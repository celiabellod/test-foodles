import pytest

from function import list_of_frequency_of_each_word


@pytest.mark.parametrize(
    "sentence, number, expected",
    [
        (
            "baz bar foo foo zblah zblah zblah baz toto bar",
            3,
            [
                ("zblah", 3),
                ("bar", 2),
                ("baz", 2),
            ],
        ),
    ],
)
def test_list_of_frequency_of_each_word(
    sentence: str, number: int, expected: list
):
    """
    Test if list_of_frequency_of_each_word should
    returns a list of size `n` where each element contains
    a word and the frequency of that word in `sentence`
    This list should be sorted by decreasing frequency and
    alphabetical order in case of tie.
    """
    frequency_of_the_sentence = list_of_frequency_of_each_word(
        sentence, number
    )

    assert len(frequency_of_the_sentence) == number
    assert frequency_of_the_sentence == expected
